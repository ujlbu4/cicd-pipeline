import pytest


@pytest.mark.unit
def test1():
    assert 0 == 0


@pytest.mark.func
def test2():
    assert 1 == 1


@pytest.mark.integ
def test3():
    assert 2 == 2
