pipeline {
    agent any

    environment {
        PATH = "/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin"
    }
    options {
        buildDiscarder(logRotator(artifactDaysToKeepStr: '', artifactNumToKeepStr: '', daysToKeepStr: '5', numToKeepStr: '10'))

        // will checkout later (also need to clean up before checkout)
        skipDefaultCheckout()
    }

    stages {
        stage('build') {
            steps {
                // Wipe the workspace so we are building completely clean
                deleteDir()

                // The checkout step will checkout code from source control;
                // scm is a special variable which instructs the checkout step to clone the specific revision which triggered this Pipeline run.
                checkout scm

                sh 'printenv'

                script {
                    // branch
                    branch_name = "${BRANCH_NAME}".replace('-', '').replace('_', '')

                    test_branch = "a-b_c".replace('-', '').replace('_', '')

                    // get version
                    def package_json = readJSON file: "package.json"
                    version = package_json['version']
                }

                echo "version=${version}"
                echo "branch=${branch_name}"
                echo "test_branch=${test_branch}"

                // build docker images
                script {
                    echo "inside other script: version=${version}"
                    sh "whoami"
                    sh "docker ps"
                    sh "which docker"
                    echo "path=${PATH}"

                    // build
                    docker.withTool("docker") {
                        jp_image = docker.build("jenkins_pipeline:${version}", "-f deploy/Dockerfile ./")
                    }

                    report_folder = "/tmp/reports"
                    echo "after docker.build"
                }
            }
        }

        stage('component tests') {
            failFast true

            parallel {
                stage('unit') {
                    steps {
                        script {
                            // run
//                            echo "path=${PATH}"
//                            echo "report_folder=${report_folder}"

                            // будет работать только на железке (а не когда докер в докере поднимается)
                            // т.е. если дженкинс будет поднят в докере, то маунты сработают для родительского
                            // (а не для текущего)
                            docker.withTool("docker") {
                                jp_image.withRun(
                                        "--name unit_tests -v ${report_folder}/unit:/tmp/alluredir",
                                        "pytest --alluredir=/tmp/alluredir --junit-xml=/tmp/alluredir/junit-report-unit.xml ./tests.py -m 'unit'"
                                ) { c ->
                                    sh "echo running unit test"
                                }
                            }

//                            jp_image.inside {
//                                // чот так и не взлетела .inside (хотя вроде все правильно)
//                                "pytest --alluredir=reports/allure-raw/ --junit-xml=/tmp/alluredir/junit-report-unit.xml ./tests.py -m 'unit'"
//                            }
                        }
                    }
                    post {
                        always {
                            // workaround
                            sh "cp -a ${report_folder} ${WORKSPACE}"

                            junit testResults: "reports/unit/junit-report-unit.xml"
                            // allure results: [[path: 'reports/unit']], commandline: "allure", report: "allure_unit"

                        }
                    }
                }
                stage('func') {
                    steps {
                        script {
                            docker.withTool("docker") {
                                jp_image.withRun(
                                        "--name func_tests -v ${report_folder}/func:/tmp/alluredir",
                                        "pytest --alluredir=/tmp/alluredir --junit-xml=/tmp/alluredir/junit-report-func.xml ./tests.py -m 'func'"
                                ) { c ->
                                    sh "echo running func test"
                                }
                            }
                        }

                        echo 'func tested'

                    }
                    post {
                        always {
                            // workaround
                            sh "cp -a ${report_folder} ${WORKSPACE}"

                            junit testResults: "reports/func/junit-report-func.xml"
                            // allure results: [[path: 'reports/func']], commandline: "allure", report: "allure_func"
                        }
                    }
                }
                stage('integ') {
                    steps {
                        script {
                            docker.withTool("docker") {
                                jp_image.withRun(
                                        "--name integ_tests -v ${report_folder}/integ:/tmp/alluredir",
                                        "pytest --alluredir=/tmp/alluredir --junit-xml=/tmp/alluredir/junit-report-integ.xml ./tests.py -m 'integ'"
                                ) { c ->
                                    sh "echo running integ test"
                                }
                            }
                        }
                        echo 'integ tested'
                    }
                    post {
                        always {
                            // workaround
                            sh "cp -a ${report_folder} ${WORKSPACE}"

                            junit testResults: "reports/integ/junit-report-integ.xml"
                            // allure results: [[path: 'reports/func']], commandline: "allure", report: "allure_func"
                        }
                    }
                }
            }

        }

        stage('publish image') {
            steps {
                echo 'publish image'
            }
        }

        stage('approve deploy stand') {
            options {
                timeout(time: 1, unit: 'DAYS')
            }
            input {
                message "Approve deployment?"
                ok "Yep"
                parameters {
                    string(name: 'STAND', defaultValue: 'space', description: 'available stands: [space, work]')
                }
            }

            steps {
                echo 'approved'
            }
        }

        stage('deploy stand') {
            steps {
                // todo: params.STAND из предыдущего стейджа не видит, видимо нужно их в scripte запихивать в глобальные
                // на этом шаге доставать то значение и использовать
                echo "previous approve stage params: ${params.STAND}"
                echo 'deployed'
            }
        }

        stage('e2e tests') {
            failFast false

            parallel {
                stage('crud') {
                    steps {
                        echo 'crud tests'
                    }
                }
                stage('acceptance') {
                    steps {
                        echo 'acceptance tests'
                    }
                }
            }
        }

        stage('approve deploy prod') {
            when { branch 'master' }
            options {
                timeout(time: 1, unit: 'DAYS')
            }
            input {
                message "Approve deployment?"
                ok "Yep"
            }

            steps {
                echo 'approved'
            }
        }

        stage('deploy prod') {
            when { branch 'master' }

            steps {
                echo 'deployed'
            }
        }
    }
}